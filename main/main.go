package main

import (
	"log"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/home_life_management/common_lib"
	"gitlab.com/home_life_management/common_lib/service/database"
	rpz "gitlab.com/home_life_management/common_lib/service/rpz_sensor"
	"gitlab.com/home_life_management/rpzsensorrecorder/config"
	"go.uber.org/zap"
)

func main() {
	conf, err := config.GetConfig()
	if err != nil {
		log.Fatalf("%+v", err)
	}

	lo := &common_lib.LoggerOption{
		LogLevel: conf.LogLevel,
	}
	logger := common_lib.NewLoggerWithOption(*lo)
	logger.Info("rpz sensor recorder started")

	logger.Debug("start connecting to DB")
	d, err := initDB(logger)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	defer d.Close()
	logger.Info("connected to DB")

	logger.Debug("start getting sensor data")
	r := rpz.NewRpzSensorService(d)

	data, err := r.GetCurrentData()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	logger.Info("got sensor data")

	logger.Debug("start inserting sensor data to DB")
	err = d.AddRpzSensorLog(*data)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	logger.Info("inserted sensor data to DB")

	logger.Info("rpz sensor recorder end successfully")
}

func initDB(logger *zap.Logger) (database.DatabaseServiceProvider, error) {
	c, _ := config.GetConfig()
	do := &database.DatabaseOption{
		Name:     c.DBName,
		User:     c.DBUser,
		Password: c.DBPassword,
	}
	d, err := database.NewPostgresService(do)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init postgres service")
	}

	err = d.Connect()
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to DB")
	}

	return d, nil
}
