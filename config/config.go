package config

import (
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/home_life_management/common_lib"
)

type Config struct {
	DBName        string `mapstructure:"DB_NAME"`
	DBUser        string `mapstructure:"DB_USER"`
	DBPassword    string `mapstructure:"DB_PASSWORD"`
	DBHost        string `mapstructure:"DB_HOST"`
	DBPort        int    `mapstructure:"DB_PORT"`
	RPZScriptPath string `mapstructure:"RPZ_SCRIPT_PATH"`
	RPZTmpPath    string `mapstructure:"RPZ_TMP_PATH"`
	LogLevel      common_lib.LogLevel
}

var conf *Config

func GetConfig() (*Config, error) {
	if conf == nil {
		err := initConfig()
		if err != nil {
			return nil, err
		}
	}
	return conf, nil
}

func initConfig() error {
	viper.BindEnv("DB_NAME")
	viper.BindEnv("DB_USER")
	viper.BindEnv("DB_PASSWORD")
	viper.BindEnv("DB_HOST")
	viper.BindEnv("DB_PORT")
	viper.BindEnv("RPZ_SCRIPT_PATH")
	viper.BindEnv("RPZ_TMP_PATH")

	err := viper.Unmarshal(&conf)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal config file")
	}
	mapLogLevel()

	return nil
}

// viperでstringをconst variableにmapする方法が不明なため実装
func mapLogLevel() {
	l := os.Getenv("LOG_LEVEL")
	switch l {
	case "DEBUG":
		conf.LogLevel = common_lib.Debug
	case "INFO":
		conf.LogLevel = common_lib.Info
	case "WARN":
		conf.LogLevel = common_lib.Warn
	case "ERROR":
		conf.LogLevel = common_lib.Error
	default:
		conf.LogLevel = common_lib.Info
	}
}
