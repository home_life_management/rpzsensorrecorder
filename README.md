# rpzSensorRecorder

RPZ-IR-Sensorから温度、気圧、湿度、明度を取得してDBに定期的に保存

## ビルド手順

* go mod tidy
* go.mod記載のgoのverが古くて警告が出る場合は必要に応じて更新
  * 例) go mod edit -go 1.20
  * 確認のため再度下記実行
    * go mod tidy
* ビルド
  * GOOS=linux GOARCH=arm go build -o rpz-sensor-recorder main/main.go
* デプロイ
  * scp rpz-sensor-recorder zero:/usr/bin/rpz-sensor-recorder
  * scp packaging/rpz-sensor-recorder.service zero:/lib/systemd/system/
  * scp packaging/rpz-sensor-recorder.timer zero:/lib/systemd/system/
  * ssh zero mkdir -p /etc/home_life_management/rpz_sensor_recorder/
  * scp rpz.conf zero:/etc/home_life_management/rpz_sensor_recorder/rpz.conf
* 起動
  * ssh zero systemctl daemon-reload
  * ssh zero systemctl enable --now rpz-sensor-recorder.timer
* 動作確認
  * timerが動作していること確認
    * ssh zero systemctl status rpz-sensor-recorder.timer
  * 1分後に、service正常実行されたこと確認
    * ssh zero systemctl status rpz-sensor-recorder.service
  * DBにデータが登録されたこと確認
    * ssh zero
    * docker exec -it life_management_db psql -d life_management -U manager
    * select * from rpz_sensor;
